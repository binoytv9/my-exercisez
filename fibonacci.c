	/* program to print 'n' fibonacci numbers or up to 'n'  */

#include<stdio.h>
#include<stdlib.h>

main()
{
	int i,j,n,fibo,choice;

	printf("\t\tMENU\n");
	printf("\t\t----\n");
	printf("\t1. 'n' fibonacci numbers\n");
 	printf("\t2. fibonacci series upto 'n'\n");
	printf("\n\t\t\tenter ur choice (1 or 2) : ");
	scanf("%d",&choice);

	switch(choice){
		case 1:
			choice=1;
			printf("\n\nhow many fibonacci number : ");
			scanf("%d",&n);
			break;
		case 2:
			choice=0;
			printf("\n\nfibonacci series upto : ");
			scanf("%d",&n);
			break;
		default:
			printf("\n\t\tInvalid option!!!\n\n");
			exit(0);
	}

	fibo=0;
	i=1;

	printf(choice ? "\n\n%d fibonacci numbers are\n" : "\n\nfibonacci series upto %d are\n",n);
	while(choice ? n-- : fibo<=n){
		j=fibo+i;
		printf("%d ",fibo);
		fibo=i;
		i=j;
	}

	printf("\n\n");
}
