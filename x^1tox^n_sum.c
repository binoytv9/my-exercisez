			/* program to print the sum x^1 + x^2 + ... + x^n */

#include<stdio.h>
#include<math.h>

#define ML 500

main()
{
	int x,n,i,sum=0;
	char exp[ML],*p=exp;

	printf("\nenter value for x : ");
	scanf("%d",&x);

	printf("\nenter value for n : ");
	scanf("%d",&n);

	for(i=1;i<=n;++i){
		sum+=pow(x,i);
	
		*p++ = x+'0';
		*p++ = '^';
		*p++ = i+'0';
		*p++ = '+';
	}
	*--p = '\0';

	printf("\n%s = %d\n\n",exp,sum);
}
