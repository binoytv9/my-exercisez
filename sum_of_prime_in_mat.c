#include<stdio.h>

int isprime(int num);

main()
{
	int i,j;
	int r,c;
	int sum=0;
	int ar[10][10];

	printf("\nenter no of rows\t: ");
	scanf("%d",&r);
	printf("\nenter no of columns\t: ");
	scanf("%d",&c);

	printf("\n\ninput the matrix :\n\n");
	for(i=0;i<r;++i)
		for(j=0;j<c;++j){
			printf("\n\tenter element ar[%d][%d] : ",i,j);
			scanf("%d",&ar[i][j]);
		}

	printf("\n\nmatrix is :\n\n");
	for(i=0;i<r;++i){
		for(j=0;j<c;++j)
			printf("\t%d ",ar[i][j]);
		
		printf("\n");
	}

	for(i=0;i<r;++i)
		for(j=0;j<c;++j)
			if(isprime(ar[i][j]))
				sum += ar[i][j];

	printf("\n\n\t\tsum of prime num is %d\n\n",sum);
}

int isprime(int num)
{
	int i;

	if(num == 1)
		return 0;

	for(i=2;i<=num/2;++i)
		if(num%i == 0)
			return 0;
	return 1;
}
