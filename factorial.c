	/* program to print factorial of a number 'n' */

#include<stdio.h>

main()
{
	int n,f=1;

	printf("\nenter the number : ");
	scanf("%d",&n);

	if(n<0)
		printf("\n\t\tfactorial undefined for %d (negative numbers !!!)\n\n",n);
	else{
		printf("\n\tfactorial of %d",n); 
		while(n)
			f*=n--;
		printf(" is %d\n\n",f);
	}
}
