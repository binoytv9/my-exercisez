/*	Write a function begins(string1,string2) that returns true if
 *	string1 begins string2. Write a program to test the function
 */


#include<stdio.h>
#include<string.h>

char *begins(char *str1, char *str2);

main()
{
	char str1[500],str2[500];

	printf("\n\nEnter the first string : ");
	fgets(str1,500,stdin);
	str1[ strlen(str1)-1 ] = '\0';

	printf("\n\nEnter the second string : ");
	fgets(str2,500,stdin);
	str2[ strlen(str2)-1 ] = '\0';
	
	printf("\n\n'%s' begins '%s' : %s\n\n",str1,str2,begins(str1,str2));
}

char *begins(char *str1, char *str2)
{
	if(strlen(str1) > strlen(str2))
		return "False";

	for(;*str1 && *str2;++str1,++str2)
		if(*str1 != *str2)
			return "False";
	return "True";
}
