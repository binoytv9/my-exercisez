#include<stdio.h>
#include<stdlib.h>
#include<string.h>


float integrate(float (*)(float),float,float);
float sqr(float);
float cube(float);

main()
{
	char c,opt[20];
	float llmt,ulmt,(*fp)(float);

	printf("\n---------MENU---------\n");
	printf("\t1. square\n\t2. cube\n");
	printf("\t\tenter ur choice : ");
	scanf("%c",&c);

	switch(c){
		case '1':
			fp=sqr;
			strcpy(opt,"square fn");
			break;
		case '2':
			fp=cube;
			strcpy(opt,"cube fn");
			break;
		default:
			printf("\nerror invalid option!!!\n");
			exit(0);
	}

	printf("\nenter the lower limit : ");
	scanf("%f",&llmt);

	printf("\nenter the upper limit : ");
	scanf("%f",&ulmt);

	printf("\nintegration of %s over the limit %.0f to %.0f is :\t%f\n\n",opt,llmt,ulmt,integrate(fp,llmt,ulmt));
}



float integrate(float (*f)(float),float llmt,float ulmt)
{
	float delta=0.0001;
	float i,sum=0;

	for(i=llmt;i<ulmt;i+=delta)
		sum+=(f(i)*delta);

	return sum;
}

float sqr(float i)
{
	return i*i;
}

float cube(float i)
{
	return i*i*i;
}
