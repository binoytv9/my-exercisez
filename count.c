/*	Write a function count(number, array, length) that counts the
 *	number of times number appears in array. The array has length elements. The
 *	function should be recursive
 */


#include<stdio.h>

int count(int n, int *a, int l);

main()
{
	int l,num;
	int ar[] = {1,2,3,4,5,2};

	num=2;
	l=sizeof(ar)/sizeof(int);
	printf("\nno of times '%d' appears in array is %d\n\n",num,count(num,ar,l));
}

int count(int n, int *a, int l)
{
	if(l == 0)
		return 0;
	else{
		if(n == *a)
			return 1 + count(n,a+1,l-1);
		else
			return 0 + count(n,a+1,l-1);
	}
}
