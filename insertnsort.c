#include<stdio.h>

#define MAX	1001		/* maximum no of elements to be sorted */

void swap(int *x,int *y);

main()
{
	int i,j,n;
	int ns=0;							/* variable to count no of shifts */
	int a[MAX];
	
	printf("\nEnter the no of elements to be sorted : ");
	scanf("%d",&n);
	
	for(i=0;i<n;++i){
		printf("\nEnter element a[%d] : ",i);
		scanf("%d",&a[i]);
	}
	
	for(i=1;i<n;++i)
		for(j=i;j && a[j]<a[j-1];--j){
			swap(&a[j],&a[j-1]);
			ns++;
		}
	printf("\nNo of shifts is %d\n",ns);
}

void swap(int *x,int *y)
{
	int t;
	t=*x;
	*x=*y;
	*y=t;
}
