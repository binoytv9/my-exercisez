/*	Write a function that takes a character array and returns a primitive
 *	hash code by adding up the value of each character in the array
 */


#include<stdio.h>

int hash(char *a);

main()
{
	char ar[] = "binoy";

	printf("\n\nhash of array is %d\n\n",hash(ar));
}

int hash(char *a)
{
	int h=0;

	while(*a){
		h += *a;
		++a;
	}
	return h;
}
