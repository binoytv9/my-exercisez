/*	Write a program which Prints the following pattern up to 10 lines
 *		0
 *	       111
 *	      22222
 *	     3333333
 */


#include<stdio.h>

main()
{
	int i,l,num;
	int space,totspace;

	printf("\n\nenter the number of line : ");
	scanf("%d",&l);

	printf("\n\n");

	totspace = (2*l + 1)/2;
	for(i=0;i<l;++i){
		num = 2*i + 1;
		space = totspace - i + 1;
		while(space--)
			printf(" ");
		while(num--)
			printf("%d",i);
		printf("\n");
	}
	printf("\n\n");
}
