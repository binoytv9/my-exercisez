/*	Write a program that converts numbers to words. For example, 895
 *	results in "eight nine five."
 */


#include<stdio.h>
#include<stdlib.h>

long reverse(long num);

main()
{
	int digit;
	long num,tmp;

	printf("\n\nEnter the number to be converted : ");
	scanf("%ld",&num);

	tmp=reverse(num);
	while(tmp > 0){
		digit = tmp%10;
		switch(digit){
			case 0:
				printf("zero");
				break;
			case 1:
				printf("one");
				break;
			case 2:
				printf("two");
				break;
			case 3:
				printf("three");
				break;
			case 4:
				printf("four");
				break;
			case 5:
				printf("five");
				break;
			case 6:
				printf("six");
				break;
			case 7:
				printf("seven");
				break;
			case 8:
				printf("eight");
				break;
			case 9:
				printf("nine");
				break;
			default :
				printf("\n\n\tInvalid number exiting ...");
				exit(0);
		}
		printf(" ");
		tmp /= 10;
	}
	printf("\n\n");
}

long reverse(long num)
{
	int rem;
	long tmp;

	while(num>0){
		rem = num%10;
		tmp = tmp*10 + rem;
		num /= 10;
	}
	return tmp;
}
