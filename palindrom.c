/* prints 2 consecutive palindrome years whose difference is DIFF */

#include<stdio.h>

#define MY	5000
#define DIFF	11

int pal(int year);

main()
{
	int yr,lyr;
	
	printf("Enter the starting year : ");
	scanf("%d",&yr);
	
	lyr=yr;
	while(yr<=MY){
		if(pal(yr)==0){
			if(yr-lyr==DIFF)
				printf("[%d\t%d]\n",lyr,yr);
			lyr=yr;
		}
		yr++;
	}
	printf("\n");
}

int pal(int year)
{
	int tmp,rev;
	
	rev=0;
	tmp=year;
	
	while(tmp){
		rev = rev*10 + tmp%10;
		tmp/=10;
	}

	return year-rev;
}
