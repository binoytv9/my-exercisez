/*	Write a function that counts the number of words in a string
 */


#include<stdio.h>
#include<ctype.h>

main()
{
	int count;
	char pre;
	char string[500],*p;

	printf("\n\nEnter the string : ");
	fgets(string,500,stdin);
	//scanf("%[^\n]s",string);

	pre = '\0';
	count = 0;
	p = string;
	while(*p){
		if(!isalpha(pre) && isalpha(*p))
			count += 1;
		pre = *p;
		++p;
	}

	printf("\n\nno of words is : %d\n\n",count);
}
