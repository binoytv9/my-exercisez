	/* program to print the following pyramid up to 'n' lines

					*
				       ***
				      *****
				     *******
				    *********				*/

#include<stdio.h>

main()
{
	int i,j,k,n;

	printf("\nenter no of lines : ");
	scanf("%d",&n);

	for(i=1,j=0;i<=n;++i,++j){
		printf("\t\t\t");

		k=n-i;
		while(k--)			/* to print white space */
			printf(" ");
		k=i+j;
		while(k--)			/* to print '*' */
			printf("*");
		printf("\n");
	}
}
